<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Mandelbrot!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>
    <section xml:id="bhax-textbook-feladatok-mandelbrot.Mandelbrot">
        <title>A Mandelbrot halmaz</title>
        <para>
            Írj olyan C programot, amely kiszámolja a Mandelbrot halmazt!     
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/gvaqijHlRUs">https://youtu.be/gvaqijHlRUs</link>
        </para>
        <para>
            Megoldás forrása:                
 <link xlink:href="Mandelbrot/mandelpngt.c++">
                <filename>Mandelbrot/mandelpngt.c++</filename>
            </link> nevű állománya.            
        </para>
        <para>
            <programlisting language="c">
                <![CDATA[#include <iostream>
#include "png++/png.hpp"
#include <sys/times.h>

#define MERET 600
#define ITER_HAT 32000]]>
            </programlisting>
        </para>
        <para>
            Meghívásra kerülnek a program futása során használt használt könyvtárak, majd definiálva lesznek a méret és az iterációs hatar.
        </para>
        <programlisting language="c">
            <![CDATA[void mandel (int kepadat[MERET][MERET]) {

    // Mérünk időt (PP 64)
    clock_t delta = clock ();
    // Mérünk időt (PP 66)
    struct tms tmsbuf1, tmsbuf2;
    times (&tmsbuf1);

    // számítás adatai
    float a = -2.0, b = .7, c = -1.35, d = 1.35;
    int szelesseg = MERET, magassag = MERET, iteraciosHatar = ITER_HAT;

    // a számítás
    float dx = (b - a) / szelesseg;
    float dy = (d - c) / magassag;
    float reC, imC, reZ, imZ, ujreZ, ujimZ;
    // Hány iterációt csináltunk?
    int iteracio = 0;
    // Végigzongorázzuk a szélesség x magasság rácsot:
    for (int j = 0; j < magassag; ++j)
    {
        //sor = j;
        for (int k = 0; k < szelesseg; ++k)
        {
            // c = (reC, imC) a rács csomópontjainak
            // megfelelő komplex szám
            reC = a + k * dx;
            imC = d - j * dy;
            // z_0 = 0 = (reZ, imZ)
            reZ = 0;
            imZ = 0;
            iteracio = 0;
            // z_{n+1} = z_n * z_n + c iterációk
            // számítása, amíg |z_n| < 2 vagy még
            // nem értük el a 255 iterációt, ha
            // viszont elértük, akkor úgy vesszük,
            // hogy a kiinduláci c komplex számra
            // az iteráció konvergens, azaz a c a
            // Mandelbrot halmaz eleme
            while (reZ * reZ + imZ * imZ < 4 && iteracio < iteraciosHatar)
            {
                // z_{n+1} = z_n * z_n + c
                ujreZ = reZ * reZ - imZ * imZ + reC;
                ujimZ = 2 * reZ * imZ + imC;
                reZ = ujreZ;
                imZ = ujimZ;

                ++iteracio;

            }

            kepadat[j][k] = iteracio;
        }
    }

    times (&tmsbuf2);
    std::cout << tmsbuf2.tms_utime - tmsbuf1.tms_utime
              + tmsbuf2.tms_stime - tmsbuf1.tms_stime << std::endl;

    delta = clock () - delta;
    std::cout << (float) delta / CLOCKS_PER_SEC << " sec" << std::endl;

}]]>
        </programlisting>
        <para>
            A program fő részét a mandel függvény alkotja. Itt mivel érdekel minket az is, hogy mennyi idő, mire lefut maga a fő program rész berakjuk egy változóba a jelenlegi időt.
            Maga a függvénynek egy bemeneti argumentuma van, a képadat, ami egy két dimenziós tömb. Ebbe a tömbbe kerül rögzítésre az algoritkus által számolt iterációk.
            Kiszámoljuk a szélesség és magasság relatív szorzóit a dx és dy válltozóba.
            Két ciklus segítségével feltöltjük a képadat tömböt úgy, hogy végig megyünk minden egyes elemen, és kiszámoljuk az iterációt.
            Ez a számolás szintél egy ciklus segítségével történik, ami addig fut, amíg 
            <function>reZ * reZ + imZ * imZ &#60; 4</function> vagy pedig amíg az iteráció el nem éri a fent meghatározott iterációs határt.
        
        </para>
        <programlisting language="c">
            <![CDATA[int main (int argc, char *argv[])
{

    if (argc != 2)
    {
        std::cout << "Hasznalat: ./mandelpng fajlnev";
        return -1;
    }

    int kepadat[MERET][MERET];

    mandel(kepadat);

    png::image < png::rgb_pixel > kep (MERET, MERET);

    for (int j = 0; j < MERET; ++j)
    {
        //sor = j;
        for (int k = 0; k < MERET; ++k)
        {
            kep.set_pixel (k, j,
                           png::rgb_pixel (255 -
                                           (255 * kepadat[j][k]) / ITER_HAT,
                                           255 -
                                           (255 * kepadat[j][k]) / ITER_HAT,
                                           255 -
                                           (255 * kepadat[j][k]) / ITER_HAT));
        }
    }

    kep.write (argv[1]);
    std::cout << argv[1] << " mentve" << std::endl;

}]]>
        </programlisting>
        <para>
            A main függvény elkészíti magát a képet a mandel függvény meghívásával.
            Ehhez feltölti az előbb említett számolással a kepadat két dimenziós tömböt adatokkal.
            Majd egy dupla for ciklus segítségével végig megy a tömb elemein, és beállítja a kép pixeleinek a szinét fehértől feketéig egy árnyalatra.
            Végül lementi a képet az első argumentumnak megadott névre.
        </para>
    

    </section>

        
        
    <section>
        <title>A Mandelbrot halmaz a <filename>std::complex</filename> osztállyal</title>
        <para>
            Írj olyan C++ programot, amely kiszámolja a Mandelbrot halmazt!                     
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/gvaqijHlRUs">https://youtu.be/gvaqijHlRUs</link>
        </para>
        <para>
            Megoldás forrása:                
        </para>
        <para>        
A <link xlink:href="#bhax-textbook-feladatok-mandelbrot.Mandelbrot">Mandelbrot halmaz</link> pontban vázolt
ismert algoritmust valósítja meg a repó <link xlink:href="../../../bhax/attention_raising/Mandelbrot/3.1.2.cpp">
                <filename>bhax/attention_raising/Mandelbrot/3.1.2.cpp</filename>
            </link> nevű állománya.
        </para>
        <para> 

        </para>

        <para>

            <programlisting language="c">

                <![CDATA[#include <complex>]]>
            </programlisting>
            Meghívásra kerül a complex könyvtárcsomag.
            A függvény megkövetel 8 argumentet (+1 maga a futtatás argumentje). Ezek az argumentek közül a szelesseg, magassag, iteraciosHatar át van konvertálva szám típussá a 
            <function>atoi</function> függvény segítségével.
            Az a, b, c, d meg át van konvertálva float típusúvá az 
            <function>atof</function> függvény segítségével.
        
        </para>
        <programlisting language="c">
            <![CDATA[  png::image < png::rgb_pixel > kep ( szelesseg, magassag );

  double dx = ( b - a ) / szelesseg;
  double dy = ( d - c ) / magassag;
  double reC, imC, reZ, imZ;
  int iteracio = 0;

  std::cout << "Szamitas\n";

  // j megy a sorokon
  for ( int j = 0; j < magassag; ++j )
    {
      // k megy az oszlopokon

      for ( int k = 0; k < szelesseg; ++k )
        {

          // c = (reC, imC) a halo racspontjainak
          // megfelelo komplex szam

          reC = a + k * dx;
          imC = d - j * dy;
          std::complex<double> c ( reC, imC );

          std::complex<double> z_n ( 0, 0 );
          iteracio = 0;

          while ( std::abs ( z_n ) < 4 && iteracio < iteraciosHatar )
            {
              z_n = z_n * z_n + c;

              ++iteracio;
            }

          kep.set_pixel ( k, j,
                          png::rgb_pixel ( iteracio%255, (iteracio*iteracio)%255, 0 ) );
        }

      int szazalek = ( double ) j / ( double ) magassag * 100.0;
      std::cout << "\r" << szazalek << "%" << std::flush;
    }

  kep.write ( argv[1] );
  std::cout << "\r" << argv[1] << " mentve." << std::endl;
]]>
        </programlisting>
        <para>
            A kep változó egy png kép típusú, aminek meg van adva maga a szélessége és magassága.
            A mehanizmus ugyan az, mint az előzőnél, kivétel, hogy itt a számolást maga a complex osztály végzi.
        </para>
    </section>
                     

    <section>
        <title>A Mandelbrot halmaz CUDA megvalósítása</title>
        <para>
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/gvaqijHlRUs">https://youtu.be/gvaqijHlRUs</link>
        </para>
        <para>
            Megoldás forrása:                
 <link xlink:href="Mandelbrot/mandelpngc_60x60_100.cu">
                <filename>Mandelbrot/mandelpngc_60x60_100.cu</filename>
            </link> nevű állománya.            
        </para>
        <para>
            A CUDA megvalósítás nagyjából megegyezik az alap C++ megvalósítással.
            Annyi különbség van, hogy a számolást a CUDA végzi.
            Lefoglalásra kerül a memóriában a device_kepadat változó.
            Ezután a mandelkernel kiszámolja a Mandelbrot hálót.
            A device_kepadat átmásolódik a kepadat váltizóba, majd felszabadítjuk a memóriából.
            
            <programlisting language='c'>
                <![CDATA[__global__ void mandelkernel (int *kepadat)
{
  int tj = threadIdx.x;
  int tk = threadIdx.y;

  int j = blockIdx.x * 10 + tj;
  int k = blockIdx.y * 10 + tk;

  kepadat[j + k * MERET] = mandel (j, k);
}

void cudamandel (int kepadat[MERET][MERET])
{
  int *device_kepadat;
  cudaMalloc ((void **) &device_kepadat, MERET * MERET * sizeof (int));

  dim3 grid (MERET / 10, MERET / 10);
  dim3 tgrid (10, 10);
  mandelkernel <<< grid, tgrid >>> (device_kepadat);  
  
  cudaMemcpy (kepadat, device_kepadat,
          MERET * MERET * sizeof (int), cudaMemcpyDeviceToHost);
  cudaFree (device_kepadat);
}]]>
            </programlisting>
        </para>
    </section>

    <section>
        <title>Mandelbrot nagyító és utazó C++ nyelven</title>
        <para>
            Építs GUI-t a Mandelbrot algoritmusra, lehessen egérrel nagyítani egy területet, illetve egy pontot
            egérrel kiválasztva vizualizálja onnan a komplex iteréció bejárta z<subscript>n</subscript> komplex számokat!
        </para>
        <para>
            Megoldás videó: 
            <link xlink:href="https://bhaxor.blog.hu/2018/09/02/ismerkedes_a_mandelbrot_halmazzal">https://bhaxor.blog.hu/2018/09/02/ismerkedes_a_mandelbrot_halmazzal</link>.        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://sourceforge.net/p/udprog/code/ci/master/tree/source/labor/Qt/Frak/">https://sourceforge.net/p/udprog/code/ci/master/tree/source/labor/Qt/Frak/</link>
        </para>
        <para>
            A megoldáshoz QT gui lett használva. A GUI létrehozást a FrakAblak osztály végzi, ez egy fix méretű ablakot hoz létre, amibe maga a fraktál képe kerül megjelenítésre.
            Magát a mandelbrot halmaz számolását a FrakSzal osztály végzi, itt hívódik meg a FrakAblak vissza függvénye, ami magát a rajzolást végzi. 
        </para>
    </section>
   
                                                                                                                                                                            
    <section>
        <title>Mandelbrot nagyító és utazó Java nyelven</title>
        <para>
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/Ui3B6IJnssY">https://youtu.be/Ui3B6IJnssY</link>, 4:27-től.
            Illetve <link xlink:href="https://bhaxor.blog.hu/2018/09/02/ismerkedes_a_mandelbrot_halmazzal">https://bhaxor.blog.hu/2018/09/02/ismerkedes_a_mandelbrot_halmazzal</link>.
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://www.tankonyvtar.hu/hu/tartalom/tkt/javat-tanitok-javat/apbs02.html#id570518">https://www.tankonyvtar.hu/hu/tartalom/tkt/javat-tanitok-javat/apbs02.html#id570518</link>
            Illetve <link xlink:href="https://progpater.blog.hu/2011/02/26/tan_csodallak_amde_nem_ertelek_de_kepzetem_hegyvolgyedet_bejarja">https://progpater.blog.hu/2011/02/26/tan_csodallak_amde_nem_ertelek_de_kepzetem_hegyvolgyedet_bejarja</link>
        </para>
        <para>
            A program több szálon fut. A halmaz számolása külön szálon történik, mivel a megjelenítés lefoglalja a fő szálat.
            A megjelenítés kép formában történik, a számolás alapján a kép pixeleinek a színei beállítódnak.
            
            <programlisting language="java">
                <![CDATA[
     public void run() {
        // A [a,b]x[c,d] tartományon milyen sűrű a
        // megadott szélesség, magasság háló:
        double dx = (b-a)/szélesség;
        double dy = (d-c)/magasság;
        double reC, imC, reZ, imZ, ujreZ, ujimZ;
        int rgb;
        // Hány iterációt csináltunk?
        int iteráció = 0;
        // Végigzongorázzuk a szélesség x magasság hálót:
        for(int j=0; j<magasság; ++j) {
            sor = j;
            for(int k=0; k<szélesség; ++k) {
                // c = (reC, imC) a háló rácspontjainak
                // megfelelő komplex szám
                reC = a+k*dx;
                imC = d-j*dy;
                // z_0 = 0 = (reZ, imZ)
                reZ = 0;
                imZ = 0;
                iteráció = 0;
                // z_{n+1} = z_n * z_n + c iterációk
                // számítása, amíg |z_n| < 2 vagy még
                // nem értük el a 255 iterációt, ha
                // viszont elértük, akkor úgy vesszük,
                // hogy a kiinduláci c komplex számra
                // az iteráció konvergens, azaz a c a
                // Mandelbrot halmaz eleme
                while(reZ*reZ + imZ*imZ < 4 && iteráció < iterációsHatár) {
                    // z_{n+1} = z_n * z_n + c
                    ujreZ = reZ*reZ - imZ*imZ + reC;
                    ujimZ = 2*reZ*imZ + imC;
                    reZ = ujreZ;
                    imZ = ujimZ;
                   
                    ++iteráció;
                   
                }
                // ha a < 4 feltétel nem teljesült és a
                // iteráció < iterációsHatár sérülésével lépett ki, azaz
                // feltesszük a c-ről, hogy itt a z_{n+1} = z_n * z_n + c
                // sorozat konvergens, azaz iteráció = iterációsHatár
                // ekkor az iteráció %= 256 egyenlő 255, mert az esetleges
                // nagyítasok során az iteráció = valahány * 256 + 255
                iteráció %= 256;
                // így a halmaz elemeire 255-255 értéket használjuk,
                // azaz (Red=0,Green=0,Blue=0) fekete színnel:
                rgb = (255-iteráció)|
                        ((255-iteráció) << 8) |
                        ((255-iteráció) << 16);
                // rajzoljuk a képre az éppen vizsgált pontot:
                kép.setRGB(k, j, rgb);
            }
            repaint();
        }
        számításFut = false;
    }
            ]]>
            </programlisting>
            A számolás ugyan úgy működik, mint a C változatában, csak itt a megjelenítés van máshogy megírva. Itt is kiszámolja az iterációs határt, és ez alapján állítja be a színeket.
        
        </para>
    </section>
</chapter>
