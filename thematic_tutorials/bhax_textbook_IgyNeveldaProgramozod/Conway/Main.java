package hu.Mark.GameOfLife;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JFrame;
import javax.swing.JPanel;

class GameRender extends JPanel {
	private int size;
	private int max;
	private int[][] table;

	public GameRender(int size) {
		this.size = size;
		this.max = 400 / size;
		this.table = new int[max][max];
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		for (int i = 0; i < this.max; i++) {
			for (int j = 0; j < this.max; j++) {
				g2.setColor(Color.black);
				if (this.table[i][j] == 1)
					g2.setColor(Color.yellow);
				g2.fillRect(i * this.size, j * this.size, this.size, this.size);
			}
		}
	}

	private int countNext(int x, int y) {
		int c = 0;
		for (int i = -1; i <= 1; i++)
			for (int j = -1; j <= 1; j++)
				if (i != 0 && j != 0 && x + i >= 0 && y + j >= 0 && x + i < this.max && y + j < this.max
						&& this.table[x + i][y + j] == 1)
					c++;
		System.out.println("cc: " + c);
		return c;
	}

	public void next() {
		for (int x = 0; x < 400 / this.size; x++) {
			for (int y = 0; y < 400 / this.size; y++) {
				// TODO: Ha nincs mellete
				if (this.table[x][y] == 1 && countNext(x, y) <= 1) {
					this.table[x][y] = 0;
				}
			}
		}
	}

	public void setRender(int x, int y, int state) {
		this.table[x][y] = state;
	}
}

public class Main extends JFrame {
	public Main() {
		this.setTitle("Game of Life");
		this.setSize(416, 438);
		this.setVisible(true);
		GameRender render = new GameRender(10);
		render.setRender(10, 33, 1);
		render.setRender(10, 32, 1);
		render.setRender(11, 32, 1);
		render.next();

		this.add(render);
	}

	public static void main(String[] args) {
		new Main();
	}
}

