<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
<info>
        <title>Helló, Chomsky!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>

    <section>
  <title>Encoding</title>
  <para>
    <command>Fordítsuk le és futtassuk a Javat tanítok könyv MandelbrotHalmazNagyító.java forrását úgy, hogy a fájl nevekben és a forrásokban is meghagyjuk az ékezetes betűket!</command>
  </para>
  <para>
    Megoldás forrása: <link xlink:href="https://www.tankonyvtar.hu/hu/tartalom/tkt/javat-tanitok-javat/adatok.html">https://www.tankonyvtar.hu/hu/tartalom/tkt/javat-tanitok-javat/adatok.html</link>.
  </para>
  <para>
    A Mandelbrot halmazt nagyítani képes programot már ismerjük tavalyról, a program működését pedig ott már elmagyaráztam. Most nem is a programkóddal fogunk foglalkozni, hanem annak a futtatásával, ami egy érdekes problémába ütközik -  karakterhasználatunk miatt nem tudjuk lefordítani a programkódot. Ennek oka pedig, hogy nem csak a latin ábécé betűiből építettük fel a kódot.
  </para>
  <para>
    Röviden a helyzet: egyszer régen, nagyon régen, a '60-as évek közepében megalkották az <citetitle>American Standard Code for Information Interchange</citetitle>-et, röviden ASCII-t. A mozaikszó már ismerős, ugye? Ez a legrégebbi, közismert kódolási forma. Ami fontos, hogy összesen 128 karaktert képes lekódolni, amelybe beletartoznak az angol ábécé betűi, 10 számjegy (0-9), valamint írás- és egyéb jeleket, viszont egyéb karakterek megjelenítésére nem képes. Ebből adódik az is, hogy a mi ékezetes magyar kakaktereinket, ha csak mi nem teszünk érte, nem fogja lefordítani ez a kódolás, ezért sorra jelentek meg az extra karaktereket is rögzítő, kiegészítő kódtáblák - ezeket ISO-8859-n kódolásként találjuk meg, ahol az n a különböző kiegészítő táblák megkülönböztetésére szolgál, ezek mind +128 karaktert adnak az eredeti karakterekhez. A magyar nyelv ábécéjét például Latin-2-es kódolás tartalmazza, ISO-8859-2 néven.
  </para>
  <para>
    A problémáinkat az Unicode UTF-8 fogja megoldani: az Unicode egy olyan rendszer, amely sokkalta több karakter ábrázolását tudja kivitelezni: a 7 biten ábrázolható ASCII karaktereket a 8 bitjével váltja fel, működése szerint a hagyományos ASCII karaktereket meghagyja eredeti formájukban, minden egyéb, ennél nagyobb kódú karakter bájtjait viszont feldaraboja, és maximum 4 bájton ábrázolja, így sokkalta nagyobb ábrázolási skálát kapunk.
  </para>
  <para>
    Térjünk rá a feladatra: a kódot a forrásban megjelölt oldalról letöltöttem egy zip csomagban, és futtatáskor a következő problémába ütköztem, amely az eddig leírtakkal magyarázható:
  </para>
  <figure>
            <title>Hiba</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="Chomsky2/100hiba.png" scale="60" />
                </imageobject>
                <textobject>
                    <phrase></phrase>
                </textobject>
            </mediaobject>
        </figure>
  <para>
    Ha nem tudnánk, hogy mi nem működik, egyszerűen ellenőrizhetjük a fájl kódolását <emphasis>file [fájlnév]</emphasis> paranccsal:
  </para>
  <figure>
            <title>Check</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="Chomsky2/check.png" scale="60" />
                </imageobject>
                <textobject>
                    <phrase></phrase>
                </textobject>
            </mediaobject>
        </figure>
  <para>
    Láthatjuk, hogy ISO-8859-ben van kódolva a program, így esélye sem volt rendesen lefutni, hiszen az alap 128 karakteren kívül mi bőven használtunk mást is. Persze, megoldás is van a problémánkra: átállítjuk még a fordítás kérésénél a kódolást. Ennek módja az alábbi képen látható: 
  </para>
  <para>
    A <emphasis>-encoding</emphasis> opcióval meg tudtuk mondani a fordítónak, hogy milyen kódolást használjon az adott forráskódra az alapértelmezett UTF-8 helyett. Most már futtatni is tudjuk a programot. Alternatív megoldás lehet az is, hogy a forrásfájlt szimplán elmentjük más, UTF-8-as kódolásban, ekkor is ugyanezt fogjuk kapni.
  </para>
    </section>


    <section>
  <title>Paszigráfia Rapszódia OpenGL full screen vizualizáció</title>
  <para>
    Lásd <link xlink:href="https://gitlab.com/nbatfai/pasigraphy-rhapsody/-/blob/master/para/docs/vis_prel_para.pdf">https://gitlab.com/nbatfai/pasigraphy-rhapsody/-/blob/master/para/docs/vis_prel_para.pdf</link>! Apró módosításokat eszközölj benne, pl. színvilág, textúrázás, a szintek jobb elkülönítése, kézreállóbb irányítás.
  </para>
  <para>
    A Paszigráfia Rapszódia projekt lényege, hogy adott egy természetes nyelvű állításunk, és ezt formalizáljuk a logika nyelvén, majd a itt szereplő változókat és szimbólumokat beszámozzuk az előfordulásuk sorrendje szerint, lehetővé téve a számok általi felírást is. A feladat megoldásaként ezeket a logikai állításokat vizuálisan fogjuk megjeleníteni 3D-s kockákként. Ezek oldalait felosztjuk négyzetrácsokra, amelyeken belül megadhatjuk, hogy hány olyan négyzetet fog tartalmazni a kocka adott oldala, amelyet kiszínezhetünk a szabályrendszerünk alapján, illetve ezek pozícióit.
  </para>
  <para>
    A feladat megoldásához először leklónoztam a projekt repóját, itt pedig a <emphasis>/para/docs</emphasis> elérési útvonalú mappában dolgozom a  <emphasis>para12.cpp</emphasis> nevű forráskóddal. A program lefordítása előtt szükségünk van néhány külső könyvtár telepítésére, amelyeket a következő parancsokkal végezhetünk el:
  </para>
  <para>
  <literallayout>
<emphasis role="strong">sudo apt-get install mesa-utils</emphasis>
<emphasis role="strong">sudo apt-get install freeglut3-dev</emphasis>
<emphasis role="strong">sudo apt-get install libsdl2-dev</emphasis>
<emphasis role="strong">sudo apt-get install libsdl2-image-dev</emphasis>
<emphasis role="strong">sudo apt-get install libboost-all-dev</emphasis>
  </literallayout>
  </para>
  <para>
    Fordítani így tudunk: <emphasis role="strong">g++ para12.cpp -o para -lboost_system -lGL -lGLU -lglut -I/usr/include/SDL2 -lSDL2 -lSDL2_image</emphasis>.
  </para>
     </section>

    <section>
  <title>Perceptron osztály</title>
  <para>
    <command>Dolgozzuk be egy külön projektbe a projekt Perceptron osztályát! Lásd https://youtu.be/XpBnR31BRJY</command>
  </para>
  <para>
    Neurális hálókkal az előző fejezetben már találkoztunk, és a most tárgyalt perceptron is egyfajta neurális háló. Egészen pontosan egy algoritmus, amely a bináris osztályozók felügyelt gépi tanulását teszi lehetővé, úgy, hogy eldöntheti, hogy egy számok vektorával ábrázolt bemenet valamely meghatározott osztályba tartozik-e vagy sem, ezt pedig mi úgy tudjuk követni, hogy 1-et ad vissza értékként, ha beletartozik és 0-t, ha nem. Az előrejelzéseit lineáris predikátor függvény alapján készíti el.
  </para>
  <para>
    A feladatunk, hogy a már ismert Mandelbrot halmazt generáló programmal generáljuk le a képet a halmazról, és adjuk meg ezt inputként a Perceptrom osztály számára. A Perceptron ezek alapján valamilyen outputot fog generálni, és a módosított értékek segítségével egy másabb, színesebb képet kapunk majd. 
  </para>
  <para>
    Ehhez a következő kódcsipetekre lesz szükségünk:
    <link xlink:href="Chomsky2/main.cpp">
      <filename>main.cpp </filename>
    </link>
    <link xlink:href="Chomsky2/mandel.cpp">
      <filename> mandel.cpp </filename>
    </link>
    <link xlink:href="Chomsky2/mlp.cpp">
      <filename> mlp.cpp </filename>
    </link>
  </para>
  <para><inlinemediaobject>
                <imageobject>
                    <imagedata fileref="Chomsky2/mandel.png" scale="50"/>
                </imageobject>
            </inlinemediaobject></para>
            <para><inlinemediaobject>
                <imageobject>
                    <imagedata fileref="Chomsky2/output.png" scale="50"/>
                </imageobject>
            </inlinemediaobject></para>
    </section>

    <section>
        <title>l334d1c4<superscript>6</superscript> (zöld - deprecated)</title>
        <para>
            Írj olyan OO Java vagy C++ osztályt, amely leet cipherként működik, azaz megvalósítja ezt a betű helyettesítést: https://simple.wikipedia.org/wiki/Leet (Ha ez első részben nem tetted meg, akkor írasd ki és magyarázd meg a használt struktúratömb memóriafoglalását!)
        </para>
        <programlisting><![CDATA[

%{
  #include <stdio.h>
  #include <stdlib.h>
  #include <time.h>
  #include <ctype.h>

  #define L337SIZE (sizeof l337d1c7 / sizeof (struct cipher))
    
  struct cipher {
    char c;
    char *leet[4];
  } l337d1c7 [] = {

  {'a', {"4", "4", "@", "/-\\"}},
  {'b', {"b", "8", "|3", "|}"}},
  {'c', {"c", "(", "<", "{"}},
  {'d', {"d", "|)", "|]", "|}"}},
  {'e', {"3", "3", "3", "3"}},
  {'f', {"f", "|=", "ph", "|#"}},
  {'g', {"g", "6", "[", "[+"}},
  {'h', {"h", "4", "|-|", "[-]"}},
  {'i', {"1", "1", "|", "!"}},
  {'j', {"j", "7", "_|", "_/"}},
  {'k', {"k", "|<", "1<", "|{"}},
  {'l', {"l", "1", "|", "|_"}},
  {'m', {"m", "44", "(V)", "|\\/|"}},
  {'n', {"n", "|\\|", "/\\/", "/V"}},
  {'o', {"0", "0", "()", "[]"}},
  {'p', {"p", "/o", "|D", "|o"}},
  {'q', {"q", "9", "O_", "(,)"}},
  {'r', {"r", "12", "12", "|2"}},
  {'s', {"s", "5", "$", "$"}},
  {'t', {"t", "7", "7", "'|'"}},
  {'u', {"u", "|_|", "(_)", "[_]"}},
  {'v', {"v", "\\/", "\\/", "\\/"}},
  {'w', {"w", "VV", "\\/\\/", "(/\\)"}},
  {'x', {"x", "%", ")(", ")("}},
  {'y', {"y", "", "", ""}},
  {'z', {"z", "2", "7_", ">_"}},
  
  {'0', {"D", "0", "D", "0"}},
  {'1', {"I", "I", "L", "L"}},
  {'2', {"Z", "Z", "Z", "e"}},
  {'3', {"E", "E", "E", "E"}},
  {'4', {"h", "h", "A", "A"}},
  {'5', {"S", "S", "S", "S"}},
  {'6', {"b", "b", "G", "G"}},
  {'7', {"T", "T", "j", "j"}},
  {'8', {"X", "X", "X", "X"}},
  {'9', {"g", "g", "j", "j"}}
  
// https://simple.wikipedia.org/wiki/Leet
  };
  
%}
%%
. {
    
    int found = 0;
    for(int i=0; i<L337SIZE; ++i)
    {
    
      if(l337d1c7[i].c == tolower(*yytext))
      {
      
        int r = 1+(int) (100.0*rand()/(RAND_MAX+1.0));
      
          if(r<91)
          printf("%s", l337d1c7[i].leet[0]);
          else if(r<95)
          printf("%s", l337d1c7[i].leet[1]);
        else if(r<98)
          printf("%s", l337d1c7[i].leet[2]);
        else 
          printf("%s", l337d1c7[i].leet[3]);

        found = 1;
        break;
      }
      
    }
    
    if(!found)
       printf("%c", *yytext);   
    
  }
%%
int 
main()
{
  srand(time(NULL)+getpid());
  yylex();
  return 0;
}


            ]]></programlisting> 
    </section>

</chapter>
