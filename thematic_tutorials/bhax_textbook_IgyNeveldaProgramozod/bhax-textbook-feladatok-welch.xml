<chapter
    xmlns="http://docbook.org/ns/docbook"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Welch!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>
    <section>
        <title>Első osztályom</title>
        <para>
            Valósítsd meg C++-ban és Java-ban az módosított polártranszformációs algoritmust! A matek háttér 
            teljesen irreleváns, csak annyiban érdekes, hogy az algoritmus egy számítása során két normálist
            számol ki, az egyiket elspájzolod és egy további logikai taggal az osztályban jelzed, hogy van vagy
            nincs eltéve kiszámolt szám.
        </para>
        <para>
            Megoldás forrása: 
            <link xlink:href="https://sourceforge.net/p/udprog/code/ci/master/tree/source/labor/polargen/">https://sourceforge.net/p/udprog/code/ci/master/tree/source/labor/polargen/</link>
        </para>
        <para>
            Tanulságok, tapasztalatok, magyarázat... térj ki arra is, hogy a JDK forrásaiban a Sun programozói
            pont úgy csinálták meg ahogyan te is, azaz az OO nemhogy nem nehéz, hanem éppen természetes neked!

            Az algoritmus lényege, hogy kiszámol egy random polártranszformációs számot. Elsőnek vissza tér az x koordinátájával, és letárolja az y koordinátát.
            Ha le van tárolva az y koordináta akkor azzal fog vissza térni, és nem számol mást. Miután vissza tért az y koordinátával elfelejti, és ha újra meghívjuk akkor újra számol egy új koordináta párt.
            Mivel két visszatérése van ezért érdemes kétszer meghívni a függvényt egymás után.

            Példa a kimenetre:
            
            <programlisting language="c">
                <![CDATA[0.389575
-0.678147
0.193406
0.937037
-1.72489
-1.07958
-0.77772
-0.00100112
-0.176438
1.19473]]>
            </programlisting>
        </para>
    </section>
    <section>
        <title>LZW</title>
        <para>
            Valósítsd meg C-ben az LZW algoritmus fa-építését!
        </para>
        <para>
            Megoldás forrása: 
            <link xlink:href="Welch/z.c">Welch/z.c</link>
        </para>
        <para>
            Az LZW egy tömörítésmentes algoritmus, az LZ78-et finomította tovább Terry Welch.
            Mivel ez egy binfa, van gyökere, és ágai. Minden ághoz tartozik egy gyökér, de nem minden gyökérnek vannak ágai.
            A lényeg, hogy a bemeneti szöveget felbontjuk bináris alakra, majd ezekből felépítjük a fát. Ha 0-ás értéket kapunk akkor létrehozunk egy bal oldali ágat,
            ha 1-es értéket kapunk akkor létrehozunk egy jobb oldali ágat.
            Ez a beolvasás egy operátorral történik, amibe a program bedobálja az átalakított bináris értékeket.
            Ennek a fának van egy mélysége is, ami igazából a gyökerek számát adja vissza.

            
            <programlisting language="c">
                <![CDATA[if (c == '0')
    {
      if (fa->bal_nulla == NULL)
        {
          fa->bal_nulla = uj_elem ();
          fa->bal_nulla->ertek = 0;
          fa->bal_nulla->bal_nulla = fa->bal_nulla->jobb_egy = NULL;
          fa = gyoker;
        }
      else
        {
          fa = fa->bal_nulla;
        }
    }
      else
    {
      if (fa->jobb_egy == NULL)
        {
          fa->jobb_egy = uj_elem ();
          fa->jobb_egy->ertek = 1;
          fa->jobb_egy->bal_nulla = fa->jobb_egy->jobb_egy = NULL;
          fa = gyoker;
        }
      else
        {
          fa = fa->jobb_egy;
        }
    }]]>
            </programlisting>
        </para>
    </section>
    <section>
        <title>Fabejárás</title>
        <para>
            Járd be az előző (inorder bejárású) fát pre- és posztorder is!
        </para>
        <para>
            Megoldás forrása: 
            <link xlink:href="Welch/z.c">Welch/z.c</link>
        </para>
        <para>
            <programlisting language="c">
                <![CDATA[//In-Order bejárás
void kiir (Csomopont * elem, std::ostream & os)
{
    if (elem != NULL)
    {
        ++melyseg;
        for (int i = 0; i < melyseg; ++i)
            os << "---";
        kiir ( elem->nullasGyermek (), os);
        os << elem->getBetu () << "(" << melyseg - 1 << ")" << std::endl;
        kiir ( elem->egyesGyermek (), os);
        --melyseg;
    }
}

//Pre-Order bejárás
void kiir (Csomopont * elem, std::ostream & os)
{
    if (elem != NULL)
    {
        ++melyseg;
        for (int i = 0; i < melyseg; ++i)
            os << "---";
        os << elem->getBetu () << "(" << melyseg - 1 << ")" << std::endl;
        kiir ( elem->nullasGyermek (), os);
        kiir ( elem->egyesGyermek (), os);
        --melyseg;
    }
}

//Post-Order bejárás
void kiir (Csomopont * elem, std::ostream & os)
{
    if (elem != NULL)
    {
        ++melyseg;
        for (int i = 0; i < melyseg; ++i)
            os << "---";
        kiir ( elem->nullasGyermek (), os);
        kiir ( elem->egyesGyermek (), os);
        os << elem->getBetu () << "(" << melyseg - 1 << ")" << std::endl;
        --melyseg;
    }
}]]>
            </programlisting>
        </para>
        <para>
            Inorder bejárás során elsőnek a bal oldali nullás gyermek kerül kiírásra. Ezután a gyökér, majd a jobb oldali egyes gyerek.
            Preorder bejárás során elsőnek maga a gyökér kerül kiírásra, majd a nullás gyermek, és végül az egyes gyermek.
            Postorder bejárás során elsőnek a bal oldali nullás gyermek kerül kiírásra, majd az egyes gyermek, végül maga a gyökér elem.
            Ezek rekurzívan vannak meghívva, és csak a kiírás sorrendje változik.
        </para>
    </section>
    <section>
        <title>Tag a gyökér</title>
        <para>
            Az LZW algoritmust ültesd át egy C++ osztályba, legyen egy Tree és egy beágyazott Node
            osztálya. A gyökér csomópont legyen kompozícióban a fával!
        </para>
        <para>
            Megoldás forrása: 
            <link xlink:href="Welch/z3a7ize.cpp">Welch/z3a7ize.cpp</link>
        </para>
        <para>
            Egy Tree osztály két darab Node osztályból áll.
            Ez a két darab node, maga a fa két ága.
            Egy Node (csomópont) így néz ki:
            
            <programlisting language="c">
                <![CDATA[Csomopont (char b = '/'):betu (b), balNulla (0), jobbEgy (0)
        {
        };
        ~Csomopont ()
        {
        };
        // Aktuális csomópont, mondd meg nékem, ki a bal oldali gyermeked
        // (a C verzió logikájával műxik ez is: ha nincs, akkor a null megy vissza)
        Csomopont *nullasGyermek () const
        {
            return balNulla;
        }
        // Aktuális csomópon,t mondd meg nékem, ki a jobb oldali gyermeked?
        Csomopont *egyesGyermek () const
        {
            return jobbEgy;
        }
        // Aktuális csomópont, ímhol legyen a "gy" mutatta csomópont a bal oldali gyereked!
        void ujNullasGyermek (Csomopont * gy)
        {
            balNulla = gy;
        }
        // Aktuális csomópont, ímhol legyen a "gy" mutatta csomópont a jobb oldali gyereked!
        void ujEgyesGyermek (Csomopont * gy)
        {
            jobbEgy = gy;
        }
        // Aktuális csomópont: Te milyen betűt hordozol?
        // (a const kulcsszóval jelezzük, hogy nem bántjuk a példányt)
        char getBetu () const
        {
            return betu;
        }
]]>
            </programlisting>

        A fában maga a gyökér is egy csomópont, ő az akihez a többi kapcsolódik. Nincs 'felette' semmi más.
        
        </para>
    </section>
    <section>
        <title>Mutató a gyökér</title>
        <para>
            Írd át az előző forrást, hogy a gyökér csomópont ne kompozícióban, csak aggregációban legyen a 
            fával!
        </para>
        <para>
            Megoldás forrása: 
            <link xlink:href="Welch/mozgatas_masolas.cpp">Welch/mozgatas_masolas.cpp</link>
        </para>
        <para>
            Első lépésként át kell írnunk a gyökeret hogy mutató legyen, ehez mindössze annyit kell tennünk hogy csillagot teszünk elé.
        </para>
        <programlisting language='c'>
            <![CDATA[Csomopont *gyoker;]]>
        </programlisting>
        <para>
            Következő lépésként, átírjuk a kódban szereplő gyoker referenciákat mutatókká, mivel maga a változó is mutató lett.
        </para>
        <para>
            Kezdésnek nem árt mondjuk a constructort átírni:
        </para>
        <programlisting language='c'>
            <![CDATA[LZWBinFa (gyoker)
{
    fa = gyoker;
}]]>
        </programlisting>
        <para>
            Ezt ha megléptük akkor sikeresen helyet foglaltunk a memóriában a csomópontnak amire a gyökér mutat, továbbá a fa mutatót is ráállítottuk a gyökérre.
        </para>
        <para>
            Mivel már pointer a gyökér ezért a szabadításnál '.' helyett '->' operátort kell alkalmaznunk, írjuk hát át ezt is!
        </para>
        <programlisting language='c'>
            <![CDATA[{
    szabadit (gyoker->egyesGyermek ());
    szabadit (gyoker->nullasGyermek ());
}]]>
        </programlisting>
        <para>
            Valamint, szintén a gyökér pointer léte miatt a kódban szereplő 
            <programlisting language='c'>
                <![CDATA["&gyoker"]]>
            </programlisting> kifejezéseket is át kell írnunk simán "gyoker"-re, ugyanis nekünk nem a pointer címére lesz szükségünk sehol hanem arra a címre amire mutat, tegyük meg hát ezeket a lépéseket is!
        
        </para>
    </section>
    <section>
        <title>Mozgató szemantika</title>
        <para>
            Írj az előző programhoz mozgató konstruktort és értékadást, a mozgató konstruktor legyen a mozgató
            értékadásra alapozva!
        </para>
        <para>
            Megoldás forrása: 
            <link xlink:href="Welch/mozgatas_masolas.cpp">Welch/mozgatas_masolas.cpp</link>
        </para>
        
        <programlisting language="c">
            <![CDATA[    LZWBinFa & operator= (const LZWBinFa & cp) {
        if(&cp != this)
            rekurzioInditasa(cp.gyoker);
        return *this;
    };]]>
        </programlisting>
        <para>
            Létrehozunk egy operátort, ami ha nem a gyökeret tartalmazza, akkor lemásolja saját magát.
            A másolás egy rekurzióval végződik, ami a fa minden ágát újra létrehozza egy másik gyökérre.
        </para>
        <programlisting language="c">
            <![CDATA[    void rekurzioInditasa(Csomopont csm){
        if(csm.nullasGyermek()){
            fa = &gyoker;
            Csomopont *uj = new Csomopont ('0');
            fa->ujNullasGyermek (uj);
            fa = fa->nullasGyermek();
            std::cout << "GYOKER: nullas van"  << std::endl;
            rekurzioAzAgakon(csm.nullasGyermek());
        }
        if(csm.egyesGyermek()){
            fa = &gyoker;
            Csomopont *uj = new Csomopont ('1');
            fa->ujEgyesGyermek (uj);
            fa = fa->egyesGyermek();
            std::cout << "GYOKER: egyes van"  << std::endl;
            rekurzioAzAgakon(csm.egyesGyermek());
        }
    }

    void rekurzioAzAgakon(Csomopont * csm){
        if (csm->nullasGyermek()) {
            std::cout << "====van nullas" << std::endl;
            Csomopont *uj = new Csomopont ('0');
            fa->ujNullasGyermek(uj);
        }
        if (csm->egyesGyermek()){
            std::cout << "====van egyes" << std::endl;
            Csomopont *uj = new Csomopont ('1');
            fa->ujEgyesGyermek(uj);
        }
        Csomopont * nullas = fa->nullasGyermek();
        Csomopont * egyes = fa->egyesGyermek();
        if(nullas){
            fa = nullas;
            rekurzioAzAgakon(csm->nullasGyermek());
        }
        if(egyes){
            fa = egyes;
            rekurzioAzAgakon(csm->egyesGyermek());
        }
    }]]>
        </programlisting>
        <para>
            A rekurzioInditasa függvény indítja el a rekurziót, ha van nullás gyermeke akkor azon fut tovább, ha van egyes gyermeke akkor arra is meghívásra kerül.
            A fő eljárást maga a rekurzioAzAgakon függvény végzi, ez fut át az összes ágon, és létrehozza az új csomópontokat.
        </para>
        <programlisting language="c">
            <![CDATA[LZWBinFa binFa2;
        binFa2 = binFa;]]>
        </programlisting>
        <para>
            A másolás már csak az egyenlőség jel operátorral meghívva történik, így az alap binFa átmásolódik a binFa2-be.
        </para>
    </section>
</chapter>
