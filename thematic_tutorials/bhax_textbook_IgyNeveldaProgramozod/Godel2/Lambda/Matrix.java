import java.util.Arrays;
import java.util.stream.IntStream;

public interface Matrix {

	void setElement(int x, int y, int value);

	Matrix multiply(Matrix input);

}
